/* keyd.c */
/*
This file is part of a program that implements a Software-Defined Radio.

Copyright (C) 2004, 2005, 2006 by Frank Brickle, AB2KT and Bob McGwier, N4HY

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

The authors can be reached by email at

ab2kt@arrl.net
or
rwmcgwier@comcast.net

or by paper mail at

The DTTS Microwave Society
6 Kathleen Place
Bridgewater, NJ 08807
*/

//#include <linux/rtc.h>
#include <errno.h>
#include <string.h>
#include <unistd.h> // sleep()

#include "fromsys.h"
#include "banal.h"
#include "splitfields.h"
#include "datatypes.h"
#include "bufvec.h"
#include "cxops.h"
#include "ringb.h"
#include "oscillator.h"
#include "cwtones.h"
#include "pthread.h"
#include "semaphore.h"
#include "keyer.h"
//#include "windows.h"
//#include "MMsystem.h"
#include "spottone.h"
#include "sdrexport.h"

#define MMRESULT unsigned int
#define UINT unsigned int
#define BOOL int
#ifndef PRIVATE
#define PRIVATE static
#endif
#define CRITICAL_SECTION pthread_mutex_t
#define LPCRITICAL_SECTION pthread_mutex_t
#define EnterCriticalSection(t) pthread_mutex_lock(t)
#define LeaveCriticalSection(t) pthread_mutex_unlock(t)
#define InitializeCriticalSectionAndSpinCount(t,c) pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_RECURSIVE_NP); \
pthread_mutex_init(t, &mutexattr); \
pthread_mutexattr_destroy(&mutexattr)
#define DeleteCriticalSection(t) pthread_mutex_destroy (t);


#define timeKillEvent(e)

pthread_mutexattr_t mutexattr;

pthread_t timerid;
pthread_t sound_thread_id;
//MMRESULT timerid = 0;

PRIVATE CRITICAL_SECTION CS_CW,UPDATE_OK,CWGEN_LOCK;
PRIVATE LPCRITICAL_SECTION *cs_cw, *update_ok, *cwgen_lock;

REAL SAMP_RATE = 48000.0;
// # times key is sampled per sec
// > 64 requires root on Linux
int key_poll_period = 1;
//#define RTC_RATE (64)
BOOLEAN HiPerformance = FALSE;

// # samples generated during 1 clock tick at RTC_RATE
//#define TONE_SIZE (SAMP_RATE / RTC_RATE)
unsigned int TONE_SIZE = 48;
unsigned int SIZEBUF = 768;
// ring buffer size; > 1 sec at this sr
//#define RING_SIZE (1<<020)
//#define RING_SIZE (1<<017)
#define RING_SIZE 2048
#define TONE_SIZE_PRM 240


KeyerState ks;
KeyerLogic kl;

static pthread_t play, key, timer;
sem_t clock_fired, keyer_started, poll_fired;

ringb_float_t *lring, *rring;

void timer_callback ();

CWToneGen cwgen;

static BOOLEAN playing = FALSE, iambic = FALSE, bug = FALSE, cw_ring_reset = FALSE;
static REAL wpm = 18.0, freq = 750.0, ramp = 5.0, gain = 1.0;



//------------------------------------------------------------

void* timer_thread(void* arg) {
    while(1) {
        sleep(key_poll_period);
        timer_callback();
    }
}

//------------------------------------------------------------


DttSP_EXP void
CWtoneExchange (float *bufl, float *bufr, int nframes)
{
	size_t numsamps, bytesize = sizeof (float) * nframes;

    ////fprintf(stderr, "\n");
    ////fprintf(stderr,"%s: nframes %d cw_ring_reset %d\n", __func__, nframes, cw_ring_reset);
    ////fprintf(stderr,"%s: lring write space %4.4d read space %4.4d\n",__func__,ringb_float_write_space(lring), ringb_float_read_space(lring));
    ////fprintf(stderr,"%s: lring size %4.4d wptr %4.4d rptr %4.4d mask 0x%4.4x\n",__func__, lring->size, lring->wptr, lring->rptr, lring->mask);

	if (cw_ring_reset)
	{
                //fprintf(stderr,"%s: cw_ring_reset=TRUE nframes=%d numsamps=%d\n",__func__,(int)nframes,(int)numsamps);
		size_t reset_size = (unsigned)RING_SIZE;
		cw_ring_reset = FALSE;
		EnterCriticalSection (cs_cw);
		ringb_float_restart (lring, reset_size);
		ringb_float_restart (rring, reset_size);
		//ringb_float_reset(lring);
		//ringb_float_reset(rring);
        ////fprintf(stderr,"%s: lring write space %d read space %d\n",__func__,ringb_float_write_space(lring), ringb_float_read_space(lring));
        ////fprintf(stderr,"%s: lring size %d wptr %d rptr %d mask 0x%4.4x\n",__func__, lring->size, lring->wptr, lring->rptr, lring->mask);
		memset (bufl, 0, bytesize);
		memset (bufr, 0, bytesize);
		LeaveCriticalSection (cs_cw);
		return;
	}
	if ((numsamps = ringb_float_read_space (lring)) < (size_t) nframes)
	{
        //fprintf(stderr,"%s: NOT ENOUGH needed nframes %d > available numsamps %d\n",__func__,(int)nframes,(int)numsamps);
		//memset (bufl, 0, bytesize);
		//memset (bufr, 0, bytesize);
		//cw_ring_reset = TRUE;
	}
	else
	{
		EnterCriticalSection (cs_cw);
        //fprintf(stderr,"%s:%d frames to YOUR BUFFER before readspace %d\n",__func__, (int)nframes, ringb_float_read_space(lring));
		ringb_float_read (lring, bufl, nframes);
		ringb_float_read (rring, bufr, nframes);
		LeaveCriticalSection (cs_cw);
		cw_ring_reset = FALSE;
	}	
}

static unsigned int xcount=0, ycount=0;

// generated tone -> output ringbuffer
void
send_tone (void)
{  
	EnterCriticalSection (cs_cw);
	if (ringb_float_write_space (lring) < TONE_SIZE)
	{
        //fprintf(stderr,"%s:< %d %d\n",__func__,ringb_float_write_space(lring), xcount);
		//cw_ring_reset = TRUE;
		xcount=0;
	}
	else
	{
		int i;
		xcount++;
        //fprintf(stderr,"%s:> %d %d\n",__func__,ringb_float_write_space(lring), xcount);
		//EnterCriticalSection (cwgen_lock);
		correctIQ(cwgen->buf, tx[1].iqfix, TRUE, 0);
		for (i = 0; i < cwgen->size; i++)
		{
			float r = (float) CXBreal (cwgen->buf, i),
				  l = (float) CXBimag (cwgen->buf, i);
			ringb_float_write (lring, (float *) &l, 1);
			ringb_float_write (rring, (float *) &r, 1);
		}
		//LeaveCriticalSection (cwgen_lock);
	}
	LeaveCriticalSection (cs_cw);
}

// silence -> output ringbuffer
void
send_silence (void)
{
	EnterCriticalSection (cs_cw);
	if (ringb_float_write_space (lring) < TONE_SIZE)
	{
        ////fprintf(stderr,"%s: %d %d\n",__func__,ringb_float_write_space(lring), ycount);
		//cw_ring_reset = TRUE;
		ycount=0;
	}
	else
	{
		int i;
		ycount++;
		//EnterCriticalSection (cwgen_lock);
        ////fprintf(stderr, "%s:\n", __func__);
		for (i = 0; i < cwgen->size; i++)
		{
			float zero = 0.0;
			ringb_float_write (lring, &zero, 1);
			ringb_float_write (rring, &zero, 1);
		}
		//LeaveCriticalSection (cwgen_lock);
    }
	LeaveCriticalSection (cs_cw);
}


// sound/silence generation
// tone turned on/off asynchronously

//CALLBACK
//timer_callback (UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1,
void timer_callback ()
{
    ////fprintf(stderr, "%s: sem_post poll_fired\n", __func__);
	sem_post (&poll_fired);
}



//DttSP_EXP void
void *
sound_thread_keyd (void *arg)
{
    //fprintf(stderr, "%s:\n", __func__);
	for (;;)
	{
		sem_wait (&clock_fired);

		if (playing)
		{
            // CWTone keeps playing for awhile after it's turned off,
			// in order to allow for a decay envelope;
			// returns FALSE when it's actually done.
            ////fprintf(stderr, "%s: cwgen freq %f size %d \n", __func__, cwgen->osc.freq, cwgen->size);
			EnterCriticalSection(cs_cw);
			playing = CWTone (cwgen);
			LeaveCriticalSection(cs_cw);
			EnterCriticalSection(update_ok);
			send_tone ();
			LeaveCriticalSection(update_ok);
		}
		else
		{
			EnterCriticalSection(update_ok);
			send_silence ();
			// only let updates run when we've just generated silence
			LeaveCriticalSection(update_ok);
		}
	}

    //fprintf(stderr, "%s: pthread_exiting\n", __func__);
	pthread_exit (0);
}


BOOLEAN
read_key (REAL del, BOOLEAN dot, BOOLEAN dash)
{
	extern BOOLEAN read_straight_key (KeyerState ks, BOOLEAN keyed);
	extern BOOLEAN read_iambic_key (KeyerState ks, BOOLEAN dot, BOOLEAN dash, KeyerLogic kl, REAL ticklen);

	if (!ks)
	{
        //fprintf(stderr,"%s: No ks! dash=%d dot=%d\n",__func__,dash,dot);
		return FALSE;
	}

	if (bug)
	{
		if (dash)
			return read_straight_key (ks, dash);
		else
			return read_iambic_key (ks, dot, FALSE, kl, del);
	}

	if (iambic)
	{
		return read_iambic_key (ks, dot, dash, kl, del);
	}
	
	return read_straight_key (ks, dot | dash);
}

/// Main keyer function,  called by a thread in the C#
BOOLEAN dotkey = FALSE;
PRIVATE BOOLEAN INLINE
whichkey (BOOLEAN dot, BOOLEAN dash)
{
	if (dotkey)
		return dot;
	return dash;
}

DttSP_EXP void
SetWhichKey (BOOLEAN isdot)
{
	if (isdot)
		dotkey = TRUE;
	else
		dotkey = FALSE;
}

// called from the hpsdr app
DttSP_EXP void
key_thread_process (REAL del, BOOLEAN dash, BOOLEAN dot, BOOLEAN keyprog)
{
	int lockers;
	BOOLEAN keydown;
	extern BOOLEAN read_straight_key (KeyerState ks, BOOLEAN keyed);

	// is the keyer ready? the keyer_started lock will be held when the keyer is ready to operate
	if(sem_getvalue(&keyer_started, &lockers))
	{
        //fprintf(stderr, "%s: sem_getvalue failed. (%s)\n", __func__, strerror(errno));
		return;
	}

	if (lockers < 1) // we assume if there is a locker, than the keyer process is ready to read the key 
		return;

	// read key; tell keyer elapsed time since last call
	if (keyprog==FALSE)
		keydown = read_key (del, dot, dash);
	else
		keydown = read_straight_key (ks, whichkey (dot, dash));

	if (!playing && keydown)
	{
		CWToneOn (cwgen);
		playing = TRUE;
	}
	else if (playing && !keydown)
	{
		CWToneOff (cwgen);
	}

	sem_post (&clock_fired);
}

DttSP_EXP BOOLEAN
KeyerPlaying ()
{
	return playing;
}

//------------------------------------------------------------------------


DttSP_EXP void
SetKeyerBug (BOOLEAN bg)
{
	EnterCriticalSection(update_ok);
	if (bg)
	{
		iambic = FALSE;
		ks->flag.mdlmdB = FALSE;
		ks->flag.memory.dah = FALSE;
		ks->flag.memory.dit = FALSE;
		bug = TRUE;
	}
	else
		bug = FALSE;
	LeaveCriticalSection(update_ok);
}

DttSP_EXP void
SetKeyerSpeed (REAL speed)
{
    //fprintf(stderr, "%s: 1SET SPEED wpm %f ks->wpm %f speed %f\n", __func__, wpm, ks->wpm, speed);
	EnterCriticalSection(update_ok);
	wpm = ks->wpm = speed;
	LeaveCriticalSection(update_ok);
    //fprintf(stderr, "%s: 2SET SPEED wpm %f ks->wpm %f speed %f\n", __func__, wpm, ks->wpm, speed);
}

DttSP_EXP void
SetKeyerWeight (int newweight)
{
	EnterCriticalSection(update_ok);
	ks->weight = newweight;
	LeaveCriticalSection(update_ok);
}

DttSP_EXP void
SetKeyerIambic (BOOLEAN setit)
{
	EnterCriticalSection(update_ok);
	if (setit)
	{
		iambic = TRUE;
		ks->flag.mdlmdB = TRUE;
		ks->flag.memory.dah = TRUE;
		ks->flag.memory.dit = TRUE;
	}
	else
	{
		iambic = FALSE;
		ks->flag.mdlmdB = FALSE;
		ks->flag.memory.dah = FALSE;
		ks->flag.memory.dit = FALSE;
	}
	LeaveCriticalSection(update_ok);
}

DttSP_EXP void
SetKeyerFreq (REAL newfreq)
{
	EnterCriticalSection(update_ok);
    //fprintf(stderr, "%s:1 newfreq %f freq %f\n", __func__, newfreq, freq);
	freq = newfreq;
	//freq = -newfreq;
    //fprintf(stderr, "%s:2 newfreq %f freq %f\n", __func__, newfreq, freq);
	setCWToneGenVals (cwgen, gain, freq, ramp, ramp);
	LeaveCriticalSection(update_ok);
}

DttSP_EXP void
SetKeyerGain (REAL newgain)
{
	if ((newgain >= 0.0) && (newgain <= 1.0))
	{
		EnterCriticalSection(update_ok);
		gain = (REAL) (20.0 * log10 (newgain));
		setCWToneGenVals (cwgen, gain, freq, ramp, ramp);
		LeaveCriticalSection(update_ok);
	}
}

DttSP_EXP void
SetKeyerRamp (REAL newramp)
{
	EnterCriticalSection(update_ok);
	ramp = newramp;
	setCWToneGenVals (cwgen, gain, freq, ramp, ramp);
	LeaveCriticalSection(update_ok);
}

DttSP_EXP void
SetKeyerMode (int newmode)
{
	EnterCriticalSection(update_ok);
	switch (newmode)
	{
		case 0:
			ks->mode = MODE_A;
			ks->flag.mdlmdB = FALSE;
			break;
		case 1:
			ks->mode = MODE_B;
			ks->flag.mdlmdB = TRUE;
			break;
		default:
			iambic = FALSE;
			break;
	}
	LeaveCriticalSection(update_ok);
}

DttSP_EXP void
SetKeyerDeBounce (int db)
{
	EnterCriticalSection(update_ok);
	ks->debounce = db;
	LeaveCriticalSection(update_ok);
}

DttSP_EXP void
SetKeyerRevPdl (BOOLEAN rvp)
{
	EnterCriticalSection(update_ok);
	ks->flag.revpdl = !rvp;
	LeaveCriticalSection(update_ok);
}

/*updateKeyer(REAL nfreq, BOOLEAN niambic, REAL ngain, REAL nramp, REAL nwpm,
			BOOLEAN revpdl, int weight, REAL SampleRate) {
	ks->flag.iambic = niambic;
	iambic = niambic;
	ks->flag.revpdl = revpdl;
	ks->weight = weight;
	wpm = nwpm;
	gain = ngain;
	ramp = nramp;
	freq = nfreq;
	cwgen->osc.freq = 2.0 * M_PI * freq / SampleRate;
} */

DttSP_EXP void
SetKeyerPerf (BOOLEAN hiperf)
{
        int rc;
	pthread_t tmp_timer;
    //fprintf(stderr,"SetKeyerPerf\n");
	tmp_timer = timerid;
	if (timerid != 0)
	{
        //fprintf(stderr,"%s: KillEvent\n", __func__);
		/*
		EnterCriticalSection(update_ok);
		timeKillEvent ((UINT) timerid);
		timerid = 0;
		//sleep (11);
		LeaveCriticalSection(update_ok);
		*/
	}
	else
	{
        //fprintf(stderr,"%s: No timerid pthread\n", __func__);
	}
		
    //fprintf(stderr,"%s: delCEToneGen\n", __func__);
	delCWToneGen (cwgen);
	if (hiperf)
	{
        //fprintf(stderr,"%s: HiPerformance TRUE\n", __func__);
		HiPerformance = TRUE;
		key_poll_period = 1;
		TONE_SIZE = 48;
	}
	else
	{
        //fprintf(stderr,"%s: HiPerformance FALSE\n", __func__);
		HiPerformance = FALSE;
		key_poll_period = 5;
		TONE_SIZE = 240;
	}

    //fprintf(stderr,"%s: gain %f freq  %f ramp %f TONE_SIZE %d SAMP_RATE %f hiperf %d\n", __func__, gain, freq, ramp, TONE_SIZE, SAMP_RATE, hiperf);

	cwgen = newCWToneGen (gain, freq, ramp, ramp, TONE_SIZE, SAMP_RATE);
	if (tmp_timer != 0)
	{
#ifndef INTERLEAVED
        //fprintf(stderr,"%s: ringb_float_restart 2\n", __func__);
		EnterCriticalSection(cs_cw);
		ringb_float_restart (lring, RING_SIZE);
		ringb_float_restart (rring, RING_SIZE);
		LeaveCriticalSection(cs_cw);
#else
        //fprintf(stderr,"%s: ringb_float_restart 1\n", __func__);
		ringb_float_restart (lring, RING_SIZE);
#endif
        //rc=pthread_create(&timerid,NULL,timer_thread,NULL);
		//if ((timerid =
		//	timeSetEvent (key_poll_period, 1,
		//	(LPTIMECALLBACK) timer_callback,
		//	(DWORD_PTR) NULL, TIME_PERIODIC)) == (MMRESULT) NULL)
        //if(rc!=0)
         //   //fprintf (stderr, "Timer failed\n"), fflush (stderr);
    }
}

DttSP_EXP void
NewKeyer (REAL freq_prm, BOOLEAN niambic, REAL gain_prm, REAL ramp, REAL wpm, REAL SampleRate)
{;

	BOOL out;

    //fprintf(stderr, "%s: freq_prm %f niambic %d gain_prm %f ramp %f wpm %f SampleRate %f\n", __func__, freq_prm, niambic, gain_prm, ramp, wpm, SampleRate);

	kl = newKeyerLogic ();
	ks = newKeyerState ();
	ks->flag.iambic = niambic;
	ks->flag.revpdl = TRUE;	// depends on port wiring
	ks->flag.autospace.khar = ks->flag.autospace.word = FALSE;
	ks->flag.mdlmdB = TRUE;
	ks->flag.memory.dah = TRUE;
	ks->flag.memory.dit = TRUE;
	ks->debounce = 1;		// could be more if sampled faster
	ks->mode = MODE_B;
	ks->weight = 50;
	ks->wpm = wpm;
	iambic = niambic;
	cs_cw = &CS_CW;
	out = InitializeCriticalSectionAndSpinCount (cs_cw, 0x00000080);
	update_ok = &UPDATE_OK;
	out = InitializeCriticalSectionAndSpinCount (update_ok, 0x00000080);
	cwgen_lock = &CWGEN_LOCK;
	out = InitializeCriticalSectionAndSpinCount (cwgen_lock, 0x00000080);
#ifndef INTERLEAVED
    //fprintf(stderr, "%s: Creating INTERLEAVED ring buffer(s) RING_SIZE %d\n", __func__, RING_SIZE);
	lring = ringb_float_create (RING_SIZE);
	rring = ringb_float_create (RING_SIZE);
	cw_ring_reset=FALSE;
	//ringb_float_restart (lring, RING_SIZE);
	//ringb_float_restart (rring, RING_SIZE);
        //fprintf(stderr,"%s: lring write space %d read space %d\n",__func__,ringb_float_write_space(lring), ringb_float_read_space(lring));
        //fprintf(stderr,"%s: lring size %d wptr %d rptr %d mask 0x%4.4x\n",__func__, lring->size, lring->wptr, lring->rptr, lring->mask);
#else
    //fprintf(stderr, "%s: Creating NON-INTERLEAVED ring buffer RING_SIZE %d\n", __func__, RING_SIZE);
	lring = ringb_float_create (2 * RING_SIZE);
#endif
	sem_init (&clock_fired, 0, 0);
	sem_init (&poll_fired, 0, 0);
	sem_init (&keyer_started, 0, 0);
	if (HiPerformance)
	{
		key_poll_period = 1;
		TONE_SIZE = 48 * (int) (uni[0].samplerate / 48000.0);
        //fprintf(stderr, "%s: High Performance CWToneGen\n", __func__, RING_SIZE);
	}
	else
	{
		key_poll_period = 5;
		TONE_SIZE = TONE_SIZE_PRM  * (int) (uni[0].samplerate / 48000.0);
	}

	freq = freq_prm;
	gain = gain_prm;

    //fprintf(stderr, "%s: gain %F freq %F ramp %F wpm %f SampleRate %f TONE_SIZE %u key_poll_period %d \n", __func__, gain, freq, ramp, wpm, SampleRate, TONE_SIZE, key_poll_period);

	//------------------------------------------------------------

	SAMP_RATE = SampleRate;
	delCWToneGen(cwgen);
	cwgen = newCWToneGen (gain, freq, ramp, ramp, TONE_SIZE, SampleRate);

	//------------------------------------------------------------
	//  if (timeSetEvent(5,1,(LPTIMECALLBACK)timer_callback,(DWORD_PTR)NULL,TIME_PERIODIC) == (MMRESULT)NULL) {
    //        //fprintf(stderr,"Timer failed\n"),fflush(stderr);
	//  }
}

/*
DttSP_EXP void
*NewCriticalSection()
{
	LPCRITICAL_SECTION cs_ptr;
	cs_ptr = (LPCRITICAL_SECTION)safealloc(1,sizeof(CRITICAL_SECTION),"Critical Section");
	return (void *)cs_ptr;
}

DttSP_EXP void
DestroyCriticalSection(LPCRITICAL_SECTION cs_ptr)
{
	safefree((char *)cs_ptr);
}
*/


DttSP_EXP void
CWRingRestart ()
{
	cw_ring_reset = TRUE;
    //fprintf(stderr,"CWRingRestart: cw_ring_reset=TRUE\n");
}

DttSP_EXP void
StartKeyer ()
{
	int rc;
    //fprintf (stderr, "StartKeyer:\n"), fflush (stderr);
	//CWRingRestart();
	rc=pthread_create(&sound_thread_id,NULL,sound_thread_keyd,NULL);
	if(rc!=0)
	{
        //fprintf (stderr, "pthread_create sound_thread_id failed\n"), fflush (stderr);
	} 	
    pthread_setname_np(sound_thread_id, "rsdk keysnd");

	rc=pthread_create(&timerid,NULL,timer_thread,NULL);
	if(rc!=0)
		//if ((timerid =
		//	timeSetEvent (key_poll_period, 1, (LPTIMECALLBACK) timer_callback,
		//	(DWORD_PTR) NULL, TIME_PERIODIC)) == (MMRESULT) NULL)
	{
        //fprintf (stderr, "Timer failed\n"), fflush (stderr);
	} 
    pthread_setname_np(timerid, "rsdk keytimer");
    sem_post (&keyer_started);
}

DttSP_EXP void
StopKeyer ()
{
////fprintf(stderr,"StopKeyer\n");
	EnterCriticalSection(update_ok);
	if (timerid)
	{
		//timeKillEvent ((UINT) timerid);
        //fprintf (stderr, "pthread_cancel timerid\n"), fflush (stderr);
		pthread_cancel(timerid);
        //fprintf (stderr, "pthread_join timerid\n"), fflush (stderr);
		pthread_join (timerid, NULL);
	}
	if (sound_thread_id)
	{
        //fprintf (stderr, "pthread_cancel sound_thread_id\n"), fflush (stderr);
		pthread_cancel(sound_thread_id);
        //fprintf (stderr, "pthread_join sound_thread_id\n"), fflush (stderr);
		pthread_join (sound_thread_id, NULL);
	}
	LeaveCriticalSection(update_ok);
	timerid = 0;
}

DttSP_EXP BOOLEAN
KeyerRunning ()
{
	return (timerid != 0);
}

DttSP_EXP void
DeleteKeyer ()
{
	StopKeyer();
//	if (clock_fired)
//	{
		sem_destroy (&clock_fired);
//		clock_fired = NULL;
//	}
//	if (poll_fired)
//	{
		sem_destroy (&poll_fired);
//		poll_fired = NULL;
//	}
//	if (keyer_started)
//	{
		sem_destroy (&keyer_started);
	//	keyer_started = NULL;
	//}
	delCWToneGen (cwgen);
	delKeyerState (ks);
	delKeyerLogic (kl);
#ifndef INTERLEAVED
	ringb_float_free (lring);
	ringb_float_free (rring);
#else
	ringb_float_free (lring);
#endif
	if (cs_cw)
	{
		DeleteCriticalSection (cs_cw);
		cs_cw = NULL;
	}
}

DttSP_EXP void
KeyerClockFireWait ()
{
	sem_wait (&clock_fired);
}

DttSP_EXP void
KeyerClockFireRelease ()
{
	sem_post (&clock_fired);
}

DttSP_EXP void
KeyerStartedWait ()
{
	sem_wait (&keyer_started);
}

DttSP_EXP void
KeyerStartedRelease ()
{
	sem_post (&keyer_started);
}

DttSP_EXP void
PollTimerWait ()
{
	sem_wait (&poll_fired);
}

DttSP_EXP void
PollTimerRelease ()
{
	sem_post (&poll_fired);
}

DttSP_EXP void
SetKeyerResetSize (unsigned int sizer)
{
	SIZEBUF = sizer;
	cw_ring_reset = TRUE;
    //fprintf(stderr,"%s: sizer %u cw_ring_reset=TRUE\n", __func__, sizer);
}

DttSP_EXP void
SetKeyerSampleRate (float sr)
{
	int factor = (int) (sr / 48000.0f);

	int lockers;
	// is the keyer ready? the keyer_started lock will be held when the keyer is ready to operate
	if(sem_getvalue(&keyer_started, &lockers))
	{
        //fprintf(stderr, "%s: sem_getvalue failed. (%s)\n", __func__, strerror(errno));
		return;
	}

	if (lockers < 1) // no lockers, then cwgen isn't ready yet 
		return;

	if (HiPerformance)
	{
		key_poll_period = 1;
		TONE_SIZE = 48 * factor;
	}
	else
	{
		key_poll_period = 5;
		TONE_SIZE = TONE_SIZE_PRM * factor;
	}
	SIZEBUF = 768 * factor;

	EnterCriticalSection (cs_cw);
	delCWToneGen (cwgen);
    //fprintf(stderr,"%s: HiPerformance %d gain %f freq %f rise %f fall %f TONE_SIZE %d sr %f\n", __func__, HiPerformance,   gain,   freq,   ramp,   ramp,   TONE_SIZE,   sr);
	cwgen = newCWToneGen (gain, freq, ramp, ramp, TONE_SIZE, sr);
	cw_ring_reset = TRUE;
	LeaveCriticalSection (cs_cw);
}

//------------------------------------------------------------------------
