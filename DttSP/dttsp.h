/** 
* @file dttsp.h
* @brief DttSP interface definitions
* @author John Melton, G0ORX/N6LYT, Doxygen Comments Dave Larsen, KV0S
* @version 0.1
* @date 2009-04-11
*/
// dttsp.h

/* Copyright (C) 
* 2009 - John Melton, G0ORX/N6LYT, Doxygen Comments Dave Larsen, KV0S
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
* 
*/

#ifndef __DTTSP_H__
#define __DTTSP_H__

typedef enum _agcmode {
    agcOFF=0, agcLONG, agcSLOW, agcMED, agcFAST
} AGCMODE;

typedef enum _sdrmode
{
  LSB,				//  0
  USB,				//  1
  DSB,				//  2
  CWL,				//  3
  CWU,				//  4
  FMN,				//  5
  AM,				//  6
  DIGU,				//  7
  SPEC,				//  8
  DIGL,				//  9
  SAM,				// 10
  DRM,				// 11
  SDRMODE_DTTSP_MAX
} SDRMODE_DTTSP;

typedef enum _metertype
{
    // RX
    SIGNAL_STRENGTH,
    AVG_SIGNAL_STRENGTH,
    ADC_REAL,
    ADC_IMAG,
    AGC_GAIN,
    // TX
    MIC,
    PWR,
    ALC,
    EQtap,
    LEVELER,
    COMP,
    CPDR,
    ALC_G,
    LVL_G,
    MIC_PK,
    ALC_PK,
    EQ_PK,
    LEVELER_PK,
    COMP_PK,
    CPDR_PK,
} METERTYPE;

#ifdef __cplusplus
extern "C" {
#endif

#define MAXRX 4
// what a mess has been made
typedef unsigned int BOOLEAN;

typedef enum _trxmode { RX, TX } TRXMODE;
typedef enum _runmode { RUN_MUTE, RUN_PASS, RUN_PLAY, RUN_SWCH } RUNMODE; 
//typedef enum _agcmode { agcOFF, agcLONG, agcSLOW, agcMED, agcFAST } AGCMODE;

extern void Setup_SDR(const char *path);
extern void Release_Update(void);
extern void SetThreadCom(int thread);
extern void Audio_Callback (float *input_l, float *input_r, float *output_l, float *output_r, unsigned int nframes, int thread);
extern void Process_ComplexSpectrum (int thread, float *results);
extern void Process_Spectrum (int thread, float *results);
extern void Process_Panadapter (int thread, float *results);
extern void Process_Phase (int thread, float *results, int numpoints);
extern void Process_Scope (int thread, float *results, int numpoints);
extern float CalculateRXMeter(int thread,unsigned int subrx, int mt);
extern float CalculateTXMeter(int thread, int mt);
extern int SetSampleRate(double sampleRate);
extern int SetRXOutputGain(unsigned int thread, unsigned subrx, double gain);
extern int SetRXPan(unsigned int thread, unsigned subrx, float pos);
extern void NewKeyer(float freq, int niambic, float gain, float ramp, float wpm, float SampleRate);
extern void SetTRX(unsigned int thread, TRXMODE setit); 
extern void SetThreadProcessingMode(unsigned int thread, RUNMODE runmode);
extern int SetSubRXSt(unsigned int thread, unsigned int subrx, BOOLEAN setit);
extern void SetMute(unsigned int thread, unsigned int subrx, BOOLEAN setit);

// AGC controls and reads
extern void SetRXAGC (unsigned int thread, unsigned int subrx, AGCMODE setit);
extern void SetRXAGCAttack(unsigned int thread, unsigned int subrx, float val);
extern void SetRXAGCDecay(unsigned int thread, unsigned int subrx, float val);
extern void SetRXAGCHang(unsigned int thread, unsigned int subrx, float val);
extern void SetRXAGCHangThreshold(unsigned int thread, unsigned int subrx, float val);
extern void SetRXAGCSlope(unsigned int thread, unsigned int subrx, float val);
extern float GetRXAGCAttack (unsigned int thread, unsigned subrx);
extern float GetRXAGCDecay (unsigned int thread, unsigned subrx);
extern float GetRXAGCHangThreshold (unsigned int thread, unsigned subrx);
extern float GetRXAGCHang (unsigned int thread, unsigned subrx);
extern float GetRXAGCSlope (unsigned int thread, unsigned subrx);

extern int SetMode (unsigned int thread, unsigned subrx, SDRMODE_DTTSP mode);

extern void SetANF(unsigned int thread, unsigned int subrx, BOOLEAN setit);
extern void SetANFValues(unsigned int thread, unsigned subrx, int taps, int delay, double gain, double leakage);

extern int SetRXFilter (unsigned int thread, unsigned int subrx, double low_frequency, double high_frequency);
extern int SetRXOsc (unsigned int thread, unsigned int subrx, double newfreq);

extern void SetBIN (unsigned int thread, unsigned int subrx, BOOLEAN setit);
extern BOOLEAN GetBIN (unsigned int thread, unsigned int subrx);

extern void SetNR (unsigned int thread, unsigned int subrx, BOOLEAN setit);
extern void SetBNR (unsigned int thread, unsigned int subrx, BOOLEAN setit);

extern void SetNB (unsigned int thread, unsigned int subrx, BOOLEAN setit);
extern BOOLEAN GetNB (unsigned int thread, unsigned int subrx);

extern void SetNBvals (unsigned int thread, unsigned int subrx, double threshold);
extern double GetNBvals (unsigned int thread, unsigned int subrx);

extern void SetNBHangtime (unsigned int thread, unsigned int subrx, double hangtime);
extern double GetNBHangtime (unsigned int thread, unsigned int subrx);

extern void SetSDROM (unsigned int thread, unsigned int subrx, BOOLEAN setit);
extern BOOLEAN GetSDROM (unsigned int thread, unsigned int subrx);

extern void SetSDROMvals (unsigned int thread, unsigned int subrx, double threshold);
extern double GetSDROMvals (unsigned int thread, unsigned int subrx);

extern void SetDSPBuflen(unsigned int thread, int buflen);

// set the snaps to use SPEC_SEMI_RAW <-> SPEC_PREMOD et al
extern void SetTXType (unsigned int thread, int setit);
extern void SetTXSpot (unsigned int thread, BOOLEAN setit);
extern void SetTXSpotFreq (unsigned int thread, double f);
extern void SetTXSpotGain (unsigned int thread, double gain);

extern void SetPhaseType (unsigned int thread, unsigned int subrx, int setit);
extern void SetScopeType (unsigned int thread, unsigned int subrx, int setit);
extern int GetScopeType (unsigned int thread, unsigned int subrx);
extern void SetSpectrumType (unsigned int thread, unsigned int subrx, int setit);
extern void SetPanadapterType (unsigned int thread, unsigned int subrx, int setit);

// set the snaps to use SPEC_MAG or SPEC_PWR
extern void SetMagmode(unsigned int thread, unsigned int subrx, int setit);

extern int SetTXFilter (unsigned int thread, double low_frequency, double high_frequency);
extern int SetTXOsc (unsigned int thread, double newfreq);
extern void SetTXAMCarrierLevel(unsigned int thread, double setit);
extern double GetTXAMCarrierLevel(unsigned int thread);
extern void SetTXCompand(unsigned int thread, double value);
extern void SetTXCompandSt(unsigned int thread, BOOLEAN setit);
extern void SetRXCompand(unsigned int thread, unsigned int subrx, double value);
extern void SetRXCompandSt(unsigned int thread, unsigned int subrx, BOOLEAN setit);
extern void SetTXDCBlock (unsigned int thread, BOOLEAN setit);
extern BOOLEAN GetTXDCBlock(unsigned int thread);
extern void SetTXAGCFF (unsigned int thread, BOOLEAN setit);
extern void SetTXAGCFFCompression (unsigned int thread, double txcompression);
extern void SetTXFMDeviation(unsigned int thread, double deviation);
extern void SetTXSquelchSt (unsigned int thread, BOOLEAN setit);
extern void SetTXSquelchVal(unsigned int thread, double value);
extern void SetTXSquelchAtt(unsigned int thread, double atten);

extern BOOLEAN GetTXALCSt(unsigned int thread);
extern void SetTXALCSt(unsigned int thread, BOOLEAN setit);
extern void SetTXALCAttack (unsigned int thread, double attack);
extern void SetTXALCDecay (unsigned int thread, double decay);
extern void SetTXALCBot (unsigned int thread, double max_agc);
extern void SetTXALCHang (unsigned int thread, double hang);

extern double GetTXALCAttack (unsigned int thread);
extern double GetTXALCDecay (unsigned int thread);
extern double GetTXALCHang (unsigned int thread);
extern double GetTXALCBot (unsigned int thread);

extern void SetTXLevelerSt (unsigned int thread, BOOLEAN setit);
extern void SetTXLevelerAttack (unsigned int thread, double attack);
extern void SetTXLevelerDecay (unsigned int thread, double decay);
extern void SetTXLevelerHang (unsigned int thread, double hang);
extern void SetTXLevelerTop (unsigned int thread, double maxgain);

extern BOOLEAN GetTXLevelerSt (unsigned int thread);
extern double GetTXLevelerAttack (unsigned int thread);
extern double GetTXLevelerDecay (unsigned int thread);
extern double GetTXLevelerHang (unsigned int thread);
extern double GetTXLevelerTop (unsigned int thread);

// uses a int[] array of 11 values;
// [0] = preamp gain
// [1] - [11] = iso band db gains
extern void SetGrphRXEQ10(unsigned int thread, unsigned int subrx, int *rxeq);
extern void SetGrphRXEQcmd (unsigned int thread, unsigned int subrx,BOOLEAN state);
extern BOOLEAN GetGrphRXEQcmd (unsigned int thread, unsigned int subrx);

extern void SetGrphTXEQ10(unsigned int thread, int *rxeq);
extern void SetGrphTXEQcmd (unsigned int thread, BOOLEAN state);
extern BOOLEAN GetGrphTXEQcmd (unsigned int thread);

extern void SetRXDCBlock(unsigned int thread, unsigned int subrx, BOOLEAN setit);
extern BOOLEAN GetRXDCBlock(unsigned int thread, unsigned int subrx);

extern void SetRXDCBlockGain(unsigned int thread, unsigned int subrx, float gain);
extern double GetRXDCBlockGain(unsigned int thread, unsigned int subrx);

extern void SetSquelchVal(unsigned int thread, unsigned int subrx, float gain);
extern float GetSquelchVal(unsigned int thread, unsigned int subrx);

extern void SetSquelchState(unsigned int thread, unsigned int subrx, BOOLEAN setit);
extern BOOLEAN GetSquelchState(unsigned int thread, unsigned int subrx);

#ifdef __cplusplus
}
#endif

#endif // __DTTSP_H__
