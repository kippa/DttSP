/* spectrum.c */

/*
This file is part of a program that implements a Software-Defined Radio.

Copyright (C) 2004, 2005, 2006 by Frank Brickle, AB2KT and Bob McGwier, N4HY

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

The authors can be reached by email at

ab2kt@arrl.net
or
rwmcgwier@comcast.net

or by paper mail at

The DTTS Microwave Society
6 Kathleen Place
Bridgewater, NJ 08807
*/

#include "spectrum.h"
#include "bufvec.h"
#include <fftw3.h>
#include "fftw3_fix.h"

CXB *get_accum(SpecBlock *sb, int type)
{
    CXB *acc;

    if (!sb)
        return NULL;

    switch (type) {
    case SPEC_PRE_FILT:
        acc = &sb->prefiltaccum;
        break;
    case SPEC_POST_FILT:
        acc = &sb->postfiltaccum;
        break;
    case SPEC_POST_AGC:
        acc = &sb->postagcaccum;
        break;
    case SPEC_POST_DET:
        acc = &sb->postdetaccum;
        break;
    case SPEC_PREMOD:
        acc = &sb->premodaccum;
        break;
    case SPEC_SEMI_RAW:
    default:
        acc = &sb->rawaccum;
    }
    return acc;
}

int get_fill(SpecBlock *sb, int type)
{
    int ret;

    if (!sb)
        return 0;

    switch (type) {
    case SPEC_PRE_FILT:
        ret = sb->prefiltfill;
        break;
    case SPEC_POST_FILT:
        ret = sb->postfiltfill;
        break;
    case SPEC_POST_AGC:
        ret = sb->postagcfill;
        break;
    case SPEC_POST_DET:
        ret = sb->postdetfill;
        break;
    case SPEC_PREMOD:
        ret = sb->premodfill;
        break;
    case SPEC_SEMI_RAW:
    default:
        ret = sb->rawfill;
    }
    return ret;
}

// snapshot of current signal
void
snap_spectrum (SpecBlock * sb, int label)
{
	int i, j;

	// where most recent signal started
    j = get_fill(sb, sb->spectrumtype);

    CXB *acc = get_accum(sb, sb->spectrumtype);

	// copy starting from there in circular fashion,
	// applying window as we go
	if (!sb->polyphase)
	{
		for (i = 0; i < sb->size; i++)
		{
            //CXBdata (sb->timebuf, i) = CXBdata (sb->accum, j);
            CXBdata (sb->timebuf, i) = Cscl (CXBdata (*acc, j), sb->window[i]);
            ++j;
            j = j & sb->mask;
		}
	}
	else
	{
		int k;
		for (i = 0; i < sb->size; i++)
		{
            CXBreal (sb->timebuf, i) = CXBreal (*acc, j) * sb->window[i];
            CXBimag (sb->timebuf, i) = CXBimag (*acc, j) * sb->window[i];
			for (k = 1; k < 8; k++)
			{
				int accumidx = (j + k * sb->size) & sb->mask;
				int winidx = i + k * sb->size;
                CXBreal (sb->timebuf, i) += CXBreal (*acc, accumidx) * sb->window[winidx];
                CXBimag (sb->timebuf, i) += CXBimag (*acc, accumidx) * sb->window[winidx];
            }
            ++j;
            j = j & sb->mask;
		}

	}
	sb->label = label;
}

void
snap_scope (SpecBlock * sb, int label)
{
	int i, j;

	// where most recent signal started
    j = get_fill(sb, sb->scopetype);

    CXB *acc = get_accum(sb, sb->scopetype);
    volatile int have = CXBhave(*acc);
    volatile int want = CXBwant(*acc);
    volatile int size = CXBsize(*acc);

    //if (have == sb->size)
    {
        // copy starting from there in circular fashion
        for (i = 0; i < sb->size; i++)
        {
            CXBdata (sb->timebuf, i) = CXBdata (*acc, j);
            ++j;
            j = j & sb->mask;
        }
    }

	sb->label = label;
}

void
compute_complex_spectrum(SpecBlock * sb)
{
	int i, j, half = sb->size / 2;

	// assume timebuf has windowed current snapshot

	fftwf_execute (sb->plan);

	for (i = 0, j = half; i < half; i++, j++) {
		sb->coutput[i] = CXBdata (sb->freqbuf, j);
		sb->coutput[j] = CXBdata (sb->freqbuf, i);
	}	
}

// snapshot -> frequency domain
void
compute_spectrum (SpecBlock * sb)
{
    int i, j, half = sb->size / 2;

    // assume timebuf has windowed current snapshot

    fftwf_execute (sb->plan);

    if (sb->scale == SPEC_MAG)
    {
        for (i = 0, j = half; i < half; i++, j++)
        {
            sb->output[i] = 100.0f * (float) Cmag (CXBdata (sb->freqbuf, j));
            sb->output[j] = 100.0f * (float) Cmag (CXBdata (sb->freqbuf, i));
        }
    }
    else
    {				// SPEC_PWR
        for (i = 0, j = half; i < half; i++, j++)
        {
            sb->output[i] = (float) (10.0f * log10f (Csqrmag (CXBdata (sb->freqbuf, j)) - 1e-60));
            sb->output[j] = (float) (10.0f * log10f (Csqrmag (CXBdata (sb->freqbuf, i)) + 1e-60));
        }
    }
}

void
init_spectrum (SpecBlock * sb)
{
	COMPLEX *p;
    sb->rawfill = 0;
    sb->prefiltfill = 0;
    sb->postfiltfill = 0;
    sb->postagcfill = 0;
    sb->postdetfill = 0;
    sb->premodfill = 0;

    p = newvec_COMPLEX_fftw(sb->size,"spectrum raw accum");
    sb->rawaccum = newCXB (sb->size , p, "spectrum raw accum");

    p = newvec_COMPLEX_fftw(sb->size,"spectrum pre filter accum");
    sb->prefiltaccum = newCXB (sb->size , p, "spectrum pre filter accum");

    p = newvec_COMPLEX_fftw(sb->size,"spectrum post filter accum");
    sb->postfiltaccum = newCXB (sb->size , p, "spectrum post filter accum");

    p = newvec_COMPLEX_fftw(sb->size,"spectrum post agc accum");
    sb->postagcaccum = newCXB (sb->size , p, "spectrum post agc accum");

    p = newvec_COMPLEX_fftw(sb->size,"spectrum post detector accum");
    sb->postdetaccum = newCXB (sb->size , p, "spectrum post detector accum");

    p = newvec_COMPLEX_fftw(sb->size,"spectrum premod accum");
    sb->premodaccum = newCXB (sb->size , p, "spectrum premod accum");

    p = newvec_COMPLEX_fftw(sb->size , "spectrum timebuf");
    sb->timebuf = newCXB (sb->size , p, "spectrum timebuf");

    p = newvec_COMPLEX_fftw(sb->size , "spectrum freqbuf");
    sb->freqbuf = newCXB (sb->size , p, "spectrum freqbuf");

    sb->window = newvec_REAL (sb->size , "spectrum window");
    sb->wintype = BLACKMANHARRIS_WINDOW;
    makewindow (sb->wintype, sb->size, sb->window);
    sb->mask = sb->size - 1;
	sb->polyphase = FALSE;
	sb->output =
		(float *) safealloc (sb->size, sizeof (float), "spectrum output");
	sb->coutput = (COMPLEX *)safealloc (sb->size, sizeof (COMPLEX), "spectrum output");;
	sb->plan =
		fftwf_plan_dft_1d (sb->size, (fftwf_complex *) CXBbase (sb->timebuf),
		(fftwf_complex *) CXBbase (sb->freqbuf),
		FFTW_FORWARD, sb->planbits);
}

void
reinit_spectrum (SpecBlock * sb)
{
	size_t polysize = 1;
	sb->fill = 0;
	if (sb->polyphase)
		polysize = 8;
    memset ((char *) CXBbase (sb->rawaccum), 0, polysize * sb->size * sizeof (REAL));
    memset ((char *) CXBbase (sb->prefiltaccum), 0, polysize * sb->size * sizeof (REAL));
    memset ((char *) CXBbase (sb->postfiltaccum), 0, polysize * sb->size * sizeof (REAL));
    memset ((char *) CXBbase (sb->postagcaccum), 0, polysize * sb->size * sizeof (REAL));
    memset ((char *) CXBbase (sb->postdetaccum), 0, polysize * sb->size * sizeof (REAL));
    memset ((char *) CXBbase (sb->premodaccum), 0, polysize * sb->size * sizeof (REAL));

	memset ((char *) sb->output, 0, sb->size * sizeof (float));
	memset ((char *) sb->coutput, 0, sb->size * sizeof(COMPLEX));
}

void
finish_spectrum (SpecBlock * sb)
{
	if (sb)
	{
        delvec_COMPLEX_fftw(sb->rawaccum->data);
        delvec_COMPLEX_fftw(sb->prefiltaccum->data);
        delvec_COMPLEX_fftw(sb->postfiltaccum->data);
        delvec_COMPLEX_fftw(sb->postagcaccum->data);
        delvec_COMPLEX_fftw(sb->postdetaccum->data);
        delvec_COMPLEX_fftw(sb->premodaccum->data);

        delCXB (sb->rawaccum);
        delCXB (sb->prefiltaccum);
        delCXB (sb->postfiltaccum);
        delCXB (sb->postagcaccum);
        delCXB (sb->postdetaccum);
        delCXB (sb->premodaccum);

        delvec_COMPLEX_fftw(sb->timebuf->data);
		delCXB (sb->timebuf);

		delvec_COMPLEX_fftw(sb->freqbuf->data);
		delCXB (sb->freqbuf);

		delvec_REAL (sb->window);
		safefree ((char *) sb->output);
		safefree ((char *) sb->coutput);
		fftwf_destroy_plan (sb->plan);
	}
}
