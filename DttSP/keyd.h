/* keyd.h */
/*
created 03/08/2014 
Kipp A. Aldrich

*/

#ifndef _KEYD_H
#define _KEYD_H


void SetKeyerSpeed (float speed);
void SetKeyerFreq (float newfreq);

#endif // _KEYD_H
